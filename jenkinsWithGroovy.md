# Jenkins with groovy script
You have to have 2 files: Jenkinsfile and script.groovy

Jenkinsfile example
```groovy
dev gv
pipeline {
    agent any
    tools {
        maven 'maven-3.9'
    }
    stages {
        stage("init") {
            steps {
                script {
                    gv = load "script.groovy"
                }
            }
        }
        stage("build jar") {
            steps {
                script {
                    gv.buildJar()
                }
            }
        }
    }
}
```

script.groovy example
```groovy
def buildJar() {
    echo "building the application..."
    sh 'mvn package'
}

return this
```