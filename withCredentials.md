# Jenkins withCredentials example

Replace idOfMyCredentials by ID from Jenkins in Manage Jenkins > Credentials > System > Global credentials > your desired service

```groovy
pipeline {
    agent any
    stages {
        stage("build image") {
            steps {
                script {
                    echo "building the docker image..."
                    withCredentials([usernamePassword(
                        credentialsId: 'idOfMyCredentials', 
                        passwordVariavble: 'PASSWD', 
                        usernameVariable: 'USER'
                        )]) {
                            /*example of use located below this comment*/
                        sh 'echo $PASSWD | docker login -u $USER --password-stdin'
                        }
                }
            }
        }
    }
}
```